﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PauseMenu : MonoBehaviour 
{
	public GUISkin mySkin;

	public bool isPaused = false;


	void Update()
	{
		if (isPaused == true)
		{
			Time.timeScale = 0;
		} 

		else
			Time.timeScale = 1;
		
	}

	public void Paused()
	{
		isPaused = true;
	}

	public void OnGUI ()
	{
		GUI.skin = mySkin;

		if (isPaused) 
		{
			GUI.Box (new Rect (Screen.width / 3, Screen.height / 3, Screen.width / 3, Screen.height / 3), "P A U S E D");

			if (GUI.Button (new Rect (Screen.width / 4 + 10, Screen.height / 4 + Screen.height / 10 + 70, Screen.width / 2 - 20, Screen.height / 10), "RESUME")) 
			{
				isPaused = false;
			}

		}
	
	}

}

