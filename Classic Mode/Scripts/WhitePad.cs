﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WhitePad : MonoBehaviour 
{
	public float speed = 10f;

	void FixedUpdate () 
	{
		float v = Input.GetAxis ("Horizontal");
		GetComponent<Rigidbody2D> ().velocity = new Vector2 (v, 0) * speed;
	}
}
