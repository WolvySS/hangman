﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ButtonScript : MonoBehaviour
{
	public void GoToPlay ()
	{
		SceneManager.LoadScene ("Classic Mode");
	}

	public void MainMenu () 
	{
		SceneManager.LoadScene ("Main Menu");
	}

	public void DualMode()
	{
		SceneManager.LoadScene ("Dual Mode"); 
	}
}
