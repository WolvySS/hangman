﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RedPadAI : MonoBehaviour {


	public float speed = 2.0f;

	public Transform bal;

	private Vector3 startpos ;


	void Start()
	{
		startpos = transform.position;
	}

	void Update()
	{
		GetComponent<Rigidbody2D> ().velocity = new Vector2 (bal.transform.position.x, 0) * speed;
		startpos += transform.position;
		//startpos.x = bal.transform.position.x;
		//startpos = transform.position;

	}

}
