﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class BallMovement1 : MonoBehaviour 
{
	public Rigidbody2D rb;
	public float speed;

	int count = 0;
	int value = 0;

	float RunTime = 0;
	float score;

	public GameObject Head;
	public GameObject Body;
	public GameObject LeftHand;
	public GameObject RightHand;
	public GameObject LeftLeg;
	public GameObject RightLeg;

	bool started;
	bool gameOver = false;
	bool scoreCalc = false;

	public GUISkin mySkin;

	void Start()
	{
		rb = GetComponent<Rigidbody2D> ();
		started = true;
	}

	void Update () 
	{
		if (Input.GetButtonDown ("Jump") && value == 0) 
		{
			scoreCalc = true;
			rb.velocity = Vector2.down * speed;
			value = 1;
		}

		if (scoreCalc) 
		{
			RunTime += Time.deltaTime;
			score = RunTime * 10;
		}

	}


	float HitFactor(Vector2 ballPos, Vector2 padPos,float padWidth )
	{
		return((ballPos.x - padPos.x) / padWidth);
	}


	void OnCollisionEnter2D (Collision2D col)
	{
		if (col.gameObject.name == "WhitePad") 
		{
			float x = HitFactor (transform.position, col.gameObject.transform.position, col.collider.bounds.size.x);
			Vector2 dir = new Vector2 (x, 1).normalized;

			GetComponent<Rigidbody2D> ().velocity = dir * speed;
		}

		else if (col.gameObject.name == "RedPad")
		{
			switch (count) 
			{
			case 0:
				Instantiate (Head); 

				break;

			case 1:
				Instantiate (Body);

				break;

			case 2:
				Instantiate (LeftHand);
				break;

			case 3:
				Instantiate (RightHand);
				break;

			case 4:
				Instantiate (LeftLeg);
				break;

			case 5:
				Instantiate (RightLeg);
				gameOver = true;
				break;
			}

			count++;

		}

	}

	void OnGUI()
	{
		GUI.skin = mySkin;

		if (started) 
		{
			GUI.Box (new Rect (Screen.width / 3, Screen.height / 3, Screen.width / 3, Screen.height / 3), "Press Space Bar To Start");

			if(Input.GetButtonDown("Jump"))
				{
				started = false;
				}
		} 

		if (!gameOver) 
		{
			GUI.Label (new Rect (10, 10, Screen.width / 6, Screen.height / 6), "Score: " + (int)score);
		}


		if (gameOver) 
		{
			Time.timeScale = 0;

			GUI.Box (new Rect (Screen.width / 3, Screen.height / 3, Screen.width / 3, Screen.height / 3), "GAME OVER\n Score: " + (int)score);

			if (GUI.Button (new Rect (Screen.width / 4 + 10, Screen.height / 4 + Screen.height / 10 + 70, Screen.width / 2 - 20, Screen.height / 10), "Restart")) 
			{
				SceneManager.LoadScene (3);
			}
		} 
	}
}


        