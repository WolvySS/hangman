﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallMovementScript : MonoBehaviour 
{
	public Rigidbody2D Rbd;
	public float speed;
	private bool started;


	public GUISkin myskin; 

	int num = 0;

	void Start () 
	{
		Rbd = GetComponent<Rigidbody2D> ();
		started = true;
	}

	void Update ()
	{
		if (Input.GetButtonDown ("Jump") && num == 0) 
		{
			Rbd.velocity = Vector2.down * speed;
			num = 1;
		}
	}

	float HitFactor(Vector2 balPos, Vector2 padsPos,float padWid )
	{
		return((balPos.x - padsPos.x) / padWid);
	}

	void OnCollisionEnter2D(Collision2D col)
	{
		if (col.gameObject.name == "Player1Pad") 
		{
			float x = HitFactor (transform.position, col.gameObject.transform.position, col.collider.bounds.size.x);
			Vector2 dir = new Vector2 (x, -1).normalized;

			GetComponent<Rigidbody2D> ().velocity = dir * speed;

		}

		else if (col.gameObject.name == "Player2Pad") 
		{
			float x = HitFactor (transform.position, col.gameObject.transform.position, col.collider.bounds.size.x);
			Vector2 dir = new Vector2 (x, 1).normalized;

			GetComponent<Rigidbody2D> ().velocity = dir * speed;

		}
			
	}

	void OnGUI()
	{
		GUI.skin = myskin;

		if (started)
		{
			GUI.Box (new Rect (Screen.width / 3, Screen.height / 3, Screen.width / 3, Screen.height / 3), "Press Space Bar To Start");

			if (Input.GetButtonDown ("Jump")) {
				started = false;
			}
		}
	}
}
