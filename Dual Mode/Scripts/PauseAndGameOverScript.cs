﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PauseAndGameOverScript : MonoBehaviour 
{
	public GUISkin mySkin;
	private bool isPaused = false;
	private bool isGameOver = false;

	void Update ()
	{
		if (Input.GetKeyDown (KeyCode.Escape) || Input.GetKeyDown (KeyCode.KeypadEnter)) 
		{
			if (isPaused)
				isPaused = false;
			else
				isPaused = true;
		}

		if (isPaused)
			Time.timeScale = 0;
		else
			Time.timeScale = 1;
	}

	public void GameOver()
	{
		isGameOver = true;
	}


	void OnGUI()
	{
		GUI.skin = mySkin;

		if (isPaused) 
		{
			GUI.Box (new Rect (Screen.width / 3, Screen.height / 3, Screen.width / 3, Screen.height / 3), "P A U S E D");

			if (GUI.Button (new Rect (Screen.width / 4 + 10, Screen.height / 4 + Screen.height / 10 + 70, Screen.width / 2 - 20, Screen.height / 10), "Restart")) 
			{
				SceneManager.LoadScene (2);
			}

		}

		if (isGameOver) 
		{
			Time.timeScale = 0;

			GUI.Box (new Rect (Screen.width / 3, Screen.height / 3, Screen.width / 3, Screen.height / 3), "G A M E   O V E R");

			if (GUI.Button (new Rect (Screen.width / 4 + 10, Screen.height / 4 + Screen.height / 10 +50, Screen.width / 2 - 20, Screen.height / 10), "Restart")) 
			{
				SceneManager.LoadScene (2);
			}

			if (GUI.Button (new Rect (Screen.width / 4 + 10, Screen.height / 4 + Screen.height / 10 + 100, Screen.width / 2 - 20, Screen.height / 10), "Exit")) 
			{
				SceneManager.LoadScene (0);
			}
				
		}
	}
}
