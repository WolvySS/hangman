﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TopEnd : MonoBehaviour
{
	public GameObject P1_Body,P1_Head,P1_LeftHand,P1_RightHand,P1_LeftLeg,P1_RightLeg;
	public PauseAndGameOverScript PGScript;

	private int Count = 0;

	void OnCollisionEnter2D(Collision2D col)
	{
		if (col.gameObject.name == "Ball_1") 
		{
			switch (Count) 
			{
			case 0:
				Instantiate (P1_Head);
				break;

			case 1:
				Instantiate (P1_Body);
				break;
			case 2:
				Instantiate (P1_LeftHand);
				break;
			case 3:
				Instantiate (P1_RightHand);
				break;
			case 4:
				Instantiate (P1_LeftLeg);
				break;
			case 5:
				Instantiate (P1_RightLeg);
				PGScript.GameOver ();
				break;
			}

			Count++;
		}
	}
}
