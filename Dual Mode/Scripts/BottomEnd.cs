﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BottomEnd : MonoBehaviour 
{
	public GameObject P2_Body, P2_Head, P2_LeftHand, P2_RightHand, P2_LeftLeg, P2_RightLeg;

	private int Count = 0;
	public PauseAndGameOverScript PGScript;

	void OnCollisionEnter2D(Collision2D col)
	{
		if(col.gameObject.name=="Ball_1")
		{
			switch (Count) 
			{
			case 0:
				Instantiate (P2_Head);
				break;

			case 1:
				Instantiate (P2_Body);
				break;

			case 2:
				Instantiate (P2_LeftHand);
				break;

			case 3:
				Instantiate (P2_RightHand);
				break;

			case 4:
				Instantiate (P2_LeftLeg);
				break;

			case 5:
				Instantiate (P2_RightLeg);
				PGScript.GameOver ();
				break;

			}

			Count++;
		}
	}

}
