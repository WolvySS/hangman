﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player2Movement : MonoBehaviour {

	public float speed = 10f;
	public string axis;

	void FixedUpdate () 
	{
		float v = Input.GetAxis ("Horizontal2");
		GetComponent<Rigidbody2D> ().velocity = new Vector2 (v, 0) * speed;
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
